<?php

class Nullor_Webgains_Block_Code extends Mage_Core_Block_Template {


    private function _getSection(){
        $pageSection  = Mage::app()->getFrontController()->getAction()->getFullActionName(); 
        return  $pageSection; 
    }

    /**
     * Get current store currency
     * @return [type]
     */
    private function _getStoreCurrency(){
        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }

	/**
	 * Renders pixel code if module is enabled
	 */
	public function _toHtml()
    {
        if (Mage::helper('nullor_webgains')->isEnabled()){
            return parent::_toHtml();
        }
    }

    public function getPurchaseEvent(){
        $pageSection        = $this->_getSection();
        $currentCurrency    = $this->_getStoreCurrency();
        
        if(Mage::helper('nullor_webgains')->isEnabled()){
        
            $pixelEvent = Mage::getModel('core/session')->getWebgainsPurchase();
            if($pixelEvent){
        
                // Mage::log('RemmotePixel - pageSection: '. $pageSection);

                // Check if standard checkout...
                /*
                if(Mage::helper('nullor_webgains')->onestepcheckoutEnabled()){ //One Step Checkout
                    if($pageSection != 'checkout_onepage_success'){
                        return;
                    }
                }
                */
            
                //Unset event
                Mage::getModel('core/session')->unsWebgainsPurchase();           
                /*
                $extra = Mage::getModel('core/session')->getData('wg_purchase');
                Mage::getSingleton('core/session')->unsetData('wg_purchase');

                $html = '<div style="width:1px; height:1px; overflow:hidden; position: absolute;"><img src="https://track.webgains.com/transaction.html?'. $extra .'" width="1" height="1" /></div>';
                return $html;
                */
                return 'webgains';
            }
            
        }
        
        return '';
    }  

}