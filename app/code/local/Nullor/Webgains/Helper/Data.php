<?php 


class Nullor_Webgains_Helper_Data extends Mage_Core_Helper_Abstract
{

    //Config paths
    const MODULE_ENABLED            = 'nullor_webgains/general/enabled';
    //const DEBUG                     = 'nullor_webgains/general/debug';
    const PROGRAM_ID                = 'nullor_webgains/general/program_id';
    const EVENT_ID                  = 'nullor_webgains/general/event_id';
    const SALT                      = 'nullor_webgains/general/salt';
    

	public function isEnabled($store = null)
    {
        return Mage::getStoreConfig(self::MODULE_ENABLED, $store);
    }

    /*
    public function isDebug($store = null)
    {
        return $this->isEnabled($store) && Mage::getStoreConfig(self::DEBUG, $store);
    }
    */

	public function getProgramId($store = null)
    {
        return Mage::getStoreConfig(self::PROGRAM_ID, $store);
    }

    public function getEventId($store = null)
    {
        return Mage::getStoreConfig(self::EVENT_ID, $store);
    }

    public function getSalt($store = null)
    {
        return Mage::getStoreConfig(self::SALT, $store);
    }

}