<?php

class Nullor_Webgains_Model_Observer
{

    /**
     * Set a flag in session when a purchase is made
     * @param  [type]     $observer
     * @return [type]
     */
    public function logWebgainsPurchase($observer){
        //Logging event
        Mage::getModel('core/session')->setWebgainsPurchase(true);

        /*
        $order = $observer->getOrder();

        $scheme = Mage::app()->getRequest()->getScheme();

        if( Mage::helper('nullor_webgains')->getProgramId() ){
            $program_id = Mage::helper('nullor_webgains')->getProgramId();
        } else {
            $program_id = '13353';
        }

        if( Mage::helper('nullor_webgains')->getEventId() ){
            $event_id = Mage::helper('nullor_webgains')->getEventId();
        } else {
            $event_id = '24873';
        }

        if( Mage::helper('nullor_webgains')->getSalt() ){
            $salt = Mage::helper('nullor_webgains')->getSalt();
        } else {
            $salt = '6063';
        }


        $items_formated = array();
        $items = $order->getAllVisibleItems();
        foreach( $items as $item ){
            $items_formated[] = $event_id.'::'.(float)$item->getPriceInclTax().'::'.$item->getName().'::'.$item->getSku().'::';
        }
        
        $params = array(
            'wgver'             => '1.2',
            'wgsubdomain'       => 'track',
            'wglang'            => Mage::app()->getLocale()->getLocaleCode(),
            'wgslang'           => 'php',
            'wgprogramid'       => $program_id,
            'wgeventid'         => $event_id,
            'wgvalue'           => $order->getGrandTotal(),
            'wgorderreference'  => $order->getIncrementId(),
            'wgcomment'         => '',
            'wgmultiple'        => '1',
            'wgitems'           => rawurlencode(implode('|',$items_formated)), // rawurlencode(implode('|', $products)),
            'wgcustomerid'      => '', // please do not use without contacting us first
            'wgproductid'       => '', // please do not use without contacting us first
            'wgvouchercode'     => ''
        );

        $params = array_merge($params,array(
            'wgchecksum'        => md5($salt . http_build_query($params)),
            'wgrs'              => '1',
            'wgprotocol'        => $scheme,
            'wgcurrency'        => $order->getOrderCurrency()->getCurrencyCode()
        )
        );

        $purchaseData = http_build_query($params);
        Mage::getSingleton('core/session')->setData('wg_purchase', $purchaseData);
        */
    }

}